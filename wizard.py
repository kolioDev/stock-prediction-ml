import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from autotrain import multi_train
from parameters import get_model_name, TICKERS
from test import test_and_predict
from yahoo_fin import stock_info as si
from datetime import date, timedelta
import random

SHOW_MOST_PROFITABLE = 4
TRAIN_TICKERS = 10


def determine_profit(ticker, print_results=True):
    future_price, loss, mean_absolute_error, accuracy_score, total_buy_profit, total_sell_profit, total_profit, profit_per_trade = test_and_predict(ticker, get_model_name(ticker), print_scores=False)
    df = si.get_data(ticker,  start_date=date.today() - timedelta(days=1) , index_as_date=False)
    last_close_price = df.at[len(df.index)-1, "close"]
    change = round((1-last_close_price/future_price) * 100, 2)
    min_change = round((1-last_close_price/(future_price-mean_absolute_error) ) * 100, 2)
    max_change = round((1-last_close_price/(future_price+mean_absolute_error) ) * 100, 2)
    if print_results:
        print(f"Future price of {ticker} after {future_price} +- {mean_absolute_error} (loss is {loss}).\
That is {change}% (with possible diff : {min_change}% to {max_change}%)")

    return change, min_change, max_change


def determine_most_profitable(tickers):
    most_profitable = []
    for ticker in tickers:
        change, min_change, max_change = determine_profit(ticker, print_results=False)
        # square avg
        avg_change = (change + min_change + max_change)/3
        if len(most_profitable) < SHOW_MOST_PROFITABLE or most_profitable[SHOW_MOST_PROFITABLE-1][0] < avg_change:
            most_profitable.append((avg_change, ticker))
            most_profitable = most_profitable[:SHOW_MOST_PROFITABLE]
            most_profitable.sort(reverse=True)
    print(most_profitable)


def predict_or_train():
    selected_tickers = random.sample(TICKERS, TRAIN_TICKERS)
    tickers_to_predict = []
    # check if model file exists
    for ticker in selected_tickers:
        if not os.path.isfile(f"./results/{get_model_name(ticker)}.h5"):
            tickers_to_predict.append(ticker)

    if len(tickers_to_predict) != 0:
        print(f"TICKERS WITH NO TRAINED MODELS - {len(tickers_to_predict)} ")
        print(tickers_to_predict)
        multi_train(tickers_to_predict)
    else:
        print("ALL TICKERS HAVE TRAINED MODELS")

    determine_most_profitable(selected_tickers)


if __name__ == "__main__":
    predict_or_train()
