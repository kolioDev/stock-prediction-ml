from train import train
from parameters import get_data_filename, get_model_name, TICKERS, SIMULTANEOUS_TRAIN
import os
import multiprocessing as mp


def combine(ticker):
    print(f"#-----------   TRAINING {ticker}   ----------#")
    try:
        train(ticker, get_data_filename(ticker), get_model_name(ticker)+"progress")
    except:
        print(f"#-----------   TRAINING {ticker} FAILED!! ----------#")
        return f"{ticker}-FAIL"

    os.rename(f"./results/{get_model_name(ticker)}progress.h5", f"./results/{get_model_name(ticker)}.h5")
    os.rename(f"./logs/{get_model_name(ticker)}progress", f"./logs/{get_model_name(ticker)}")
    return ticker


def multi_train(tickers):
    pool = mp.Pool(processes=SIMULTANEOUS_TRAIN)
    results = pool.starmap(combine, zip(tickers))
    print(f"SUCCESSFULLY TRAINED {results} ({len(results)} of {len(tickers)})")


if __name__ == "__main__":
    multi_train(TICKERS)
