import os
import time
from tensorflow.keras.layers import LSTM

# Set time zone to 'America/New_York'
os.environ['TZ'] = 'America/New_York'
try:
    time.tzset()
except:
    print("Waring: cannot set timezone")

# Window size or the sequence length
N_STEPS = 300
# Lookup step, 1 is the next day
LOOKUP_STEP = 5

# whether to scale feature columns & output price as well
SCALE = True
scale_str = f"sc-{int(SCALE)}"
# whether to shuffle the dataset
SHUFFLE = True
shuffle_str = f"sh-{int(SHUFFLE)}"
# whether to split the training/testing set by date
SPLIT_BY_DATE = False
split_by_date_str = f"sbd-{int(SPLIT_BY_DATE)}"
# test ratio size, 0.2 is 20%
TEST_SIZE = 0.2
# features to use
FEATURE_COLUMNS = ["adjclose", "volume", "open", "high", "low"]
# date now
date_now = time.strftime("%Y-%m-%d")

### model parameters

N_LAYERS = 3
# LSTM cell
CELL = LSTM
# 256 LSTM neurons
UNITS = 256
# 40% dropout
DROPOUT = 0.3
# whether to use bidirectional RNNs
BIDIRECTIONAL = False

### training parameters

# mean absolute error loss
# LOSS = "mae"
# huber loss
LOSS = "huber_loss"
OPTIMIZER = "adam"
BATCH_SIZE = 64
EPOCHS = 500

# Amazon stock market
ticker = "EA"


def get_data_filename(ticker):
    return os.path.join("data", f"{ticker}_{date_now}.csv")


ticker_data_filename = get_data_filename(ticker)


# model name to save, making it as unique as possible based on parameter
def get_model_name(ticker):
    model_name =  f"{date_now}_{ticker}-{shuffle_str}-{scale_str}-{split_by_date_str}-{LOSS}-{OPTIMIZER}-{CELL.__name__}-seq-{N_STEPS}-step-{LOOKUP_STEP}-layers-{N_LAYERS}-units-{UNITS}"
    if BIDIRECTIONAL:
        model_name += "-b"
    return model_name


model_name = get_model_name(ticker)


TICKERS = [
    "AAPL",
    "TSLA",
    "NIO",
    "NFLX",
    "KO",
    "DIS",
    "NCLH",
    "ZM",
    "CCL",
    "INTC",
    "IVR",
    "T",
    "UBER",
    "SHOP",
    "MA",
    "V",
    "MCD",
    "WORK",
    "GPRO",
    "SNE",
    "ADBE",
    "ABNB",
    "AI",
    "DASH",
    "DKNG",
    "Li",
    "ROOT",
    "WKHS",
    "APPS",
    "SQ",
    "SPWR",
    "JKS",
    "SNOW",
    "ATVI",
    "BIDU",
    "ZNGA",
    "TWTR",
    "GRPN",
    "TWLO",
    "TSM",
    "EA",
    "RUN",
    "CSCO",
    "SPLK",
    "MDB",
    "MOMO",
    "SUMO",
    "TEAM",
    "IRBT",
    "ADSK",
    "QLYS"
    "PFPT"
]

SIMULTANEOUS_TRAIN = 4